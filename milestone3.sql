Drop table if exists milestone2.passengers;
Drop table if exists milestone2.employees;
Drop table if exists milestone2.employeeAddresses;
Drop table if exists milestone2.maintenance;
Drop table if exists milestone2.flights;
Drop table if exists milestone2.airports;
Drop table if exists milestone2.payments;
Drop table if exists milestone2.users;
Drop table if exists milestone2.tickets;
Drop table if exists milestone2.planes;
Drop table if exists milestone2.legs;
Drop table if exists milestone2.ticketlegs;


DROP TRIGGER if exists milestone2.ticket_price_check;
DROP TRIGGER if exists milestone2.flights_seatsRemaining_check;
DROP TRIGGER if exists milestone2.employee_legalize_wage;

Drop database if exists milestone2;


create database milestone2;
use milestone2;

CREATE TABLE passengers (
	passengerID VARCHAR(9),
	ticketID VARCHAR(9),
	lName VARCHAR(50),
	fName VARCHAR(25),
	dob VARCHAR(10),
	PRIMARY KEY(passengerID),
	FOREIGN KEY(ticketID)
	REFERENCES tickets(ticketID)
	ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE airports (
	airportID VARCHAR(9), 
	numDockingBays VARCHAR(3), 
	airportName VARCHAR(50), 
	airportAddress VARCHAR(50), 
	airportCity VARCHAR(50), 
	airportState VARCHAR(50), 
	airportZIP VARCHAR(6), 
	airportCountry VARCHAR(50),
	PRIMARY KEY(airportID)
);

CREATE TABLE planes(
	planeID VARCHAR(9),
    passengerCapacity int,
    manufactureDate date,
    manufacturer VARCHAR(25),
    model VARCHAR(25),
    seatsPerRow int, /* So this might be inconsistent..  */
    totalRows int, 
    fuelCapacity float,
	PRIMARY KEY(planeID)
);

CREATE TABLE employees (
	employeeID VARCHAR(9),
	supervisorID VARCHAR(9),
	lName VARCHAR(50),
	fName VARCHAR(25),
	dob VARCHAR(10),
	wage DOUBLE(12,2),
	employeeType VARCHAR(25),
	PRIMARY KEY(employeeID)
);

CREATE TABLE employeeAddresses(
	employeeID VARCHAR(9),
	address VARCHAR(30),
	city VARCHAR(20),
	stateName VARCHAR(15),
	zip VARCHAR(5),
	country VARCHAR(15),
	PRIMARY KEY(employeeID)
);

CREATE TABLE users (
	userID VARCHAR(9), 
	freqUser BOOLEAN, 
	email VARCHAR(50), 
	password VARCHAR(24), 
	fName VARCHAR(25), 
	lName VARCHAR(50),
	PRIMARY KEY(userID)
);

CREATE TABLE maintenance (
	maintenanceID VARCHAR(9),
	eventType VARCHAR(25),
	airportID VARCHAR(9),
	planeID VARCHAR(9),
	employeeID VARCHAR(9),
	date VARCHAR(10),
	time VARCHAR(5),
	status VARCHAR(10),
	comments VARCHAR(250),
	PRIMARY KEY(maintenanceID),
	FOREIGN KEY(employeeID)
	REFERENCES employees(employeeID)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(airportID)
	REFERENCES airports(airportID)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(planeID)
	REFERENCES planes(planeID)
	ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE payments (
	paymentID VARCHAR(9), 
	passengerID VARCHAR(9), 
	userID VARCHAR(9), 
	amount DOUBLE(9,2), 
	status VARCHAR(50),
	PRIMARY KEY(paymentID),
	FOREIGN KEY(passengerID) REFERENCES passengers(passengerID)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(userID)	REFERENCES users(userID)
	ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE legs(
	legID VARCHAR(9),
    departureAirportID VARCHAR(9),
    arrivalAirportID VARCHAR(9),
    flightID VARCHAR(9),
    PRIMARY KEY(legID),
	FOREIGN KEY(departureAirportID)
	REFERENCES airports(airportID)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(arrivalAirportID)
	REFERENCES airports(airportID)
	ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE ticketLegs(
	routeID VARCHAR(9),
    ticketID VARCHAR(9)
);

CREATE TABLE tickets (
	passengerID VARCHAR(9),
	ticketID VARCHAR(9),
	userID VARCHAR(9),
	paymentID VARCHAR(9),
	flightID VARCHAR(9),
	seatNumber INTEGER(3),
	rowNumber INTEGER(3),
	price DOUBLE(9,2),
	fName VARCHAR(25),
	dateBooked VARCHAR(10),
	PRIMARY KEY(ticketID),
	FOREIGN KEY(passengerID)
	REFERENCES passengers(passengerID)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(userID)
	REFERENCES users(userID)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(paymentID)
	REFERENCES payments(paymentID)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(flightID)
	REFERENCES flights(flightID)
	ON UPDATE CASCADE ON DELETE CASCADE
);

delimiter //
CREATE TRIGGER ticket_price_check 
BEFORE INSERT on tickets
	For each row
	BEGIN
		IF NEW.price > 1000 
			THEN
				SET NEW.ticketID = 999.00;
		End if;
	end;//
delimiter ;

delimiter //
CREATE TRIGGER flights_seatsRemaining_check 
BEFORE INSERT on flights
	For each row
	BEGIN
		IF NEW.seatsRemaining < 0 
			THEN
				SET NEW.seatsRemaining = 0;
		End if;
	end;//
delimiter ;

delimiter //
CREATE TRIGGER employee_legalize_wage
BEFORE INSERT on employees
	for each row
	BEGIN 
		IF NEW.wage < 8.25
			THEN
				SET NEW.wage = 8.25;
		END if;
	end;//
delimiter ;

INSERT INTO passengers 
VALUES('123456789','999','Bailey','Douglas','01/02/1985');
INSERT INTO passengers 
VALUES('123456788','11111111','Dylan','Kehres','07/25/1996');

INSERT INTO airports 
VALUES('555555555','9','Ohare International','123 Ohare St','Chicago','IL', '61601','USA');
INSERT INTO airports 
VALUES('444444444','0','Indianapolis International','111 Harding St','Indianapolis', 'IN','46142','USA');

INSERT INTO planes
VALUES('937465836', '524','2004-10-08','BOEING', '747', '10','67','48445');
INSERT INTO planes
VALUES('937465837', '524','2004-09-08','BOEING', '747', '10','67','48445');

INSERT INTO employees 
VALUES('333333333','878787878','Mark','Johnson','01/02/1988','40000.00', 'Maintenance');
INSERT INTO employees 
VALUES('878787878', NULL,'Bob','Ross','06/04/1980','60000.00', 'Maintenance Supervisor');

INSERT INTO employeeAddresses
VALUES('333333333','123 Main St','Peoria','IL','61606','USA');
INSERT INTO employeeAddresses
VALUES('878787878','101 University St','Peoria','IL','61606','USA');

INSERT INTO users 
VALUES('010101010','0','dbailey@gmail.com','Oldsch00l','Doug','Bailey');
INSERT INTO users 
VALUES('101010101','1','dkehres@gmail.com','fence','Dylan','Kehres');

INSERT INTO payments 
VALUES('999999999','123456789','111111111','500.00','Paid');
INSERT INTO payments 
VALUES('999999998','123456788','111111110','1000.00','Unpaid');

INSERT INTO legs
VALUES ('745393725', '555555555', '444444444', '121212121');
INSERT INTO legs
VALUES ('745393726', '444444444', '555555555', '212121212');

INSERT INTO ticketLegs
VALUES ('RouteID', '123456789');
INSERT INTO ticketLegs
VALUES ('RouteID', '123456788');

INSERT INTO tickets
VALUES ('123456789', '987654321','010101010','666666666','777777777','888888888','1','1','300.00','Doug','4/19/2017');
INSERT INTO tickets
VALUES ('123456788', '123456789','101010101','666666665','777777776','888888887','1','2','3000.00','Dylan','4/18/2017');

INSERT INTO flights 
VALUES('121212121','343434343','5','000000001','05/02/2017','12:00', '15:00','0','On time');
INSERT INTO flights 
VALUES('212121212','434343434','0','000000002','05/04/2017','10:00', '14:00','1','On time');

INSERT INTO maintenance
VALUES('11111111','Engine Repair','555555555','937465836','333333333','01/25/2017','16:00','Fixed','New Engine');
INSERT INTO maintenance
VALUES('11111112','Wing Repair','444444444','937465837','333333333','02/25/2017','12:00','Fixed','Hole patched');

SELECT * FROM passengers;
SELECT * FROM tickets;

SELECT * FROM legs WHERE departureAirportID = '555555555' OR arrivalAirportID = '444444444';
SELECT legID FROM legs;

SELECT * FROM maintenance WHERE planeID = '937465837';
SELECT fName, lName FROM employees WHERE employeeType = 'Maintenance';

SELECT * FROM flights WHERE remainingSeats > 0;
SELECT status FROM flights WHERE planeID - "11111111";